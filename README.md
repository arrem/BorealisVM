# BorealisVM
 A small stack-based virtual machine for an Abstract Machines course at TU Vienna, 2018. 

## Reading the code

This section covers all of the important bits of the code that an interested reader should have a look at. :)

We aim to provide a short tour, showcasing all the aspects of the code, without causing too much effort for the reader.
The section does however assume basic familiarity with the implementation of the machine. These details are described
in `SPEC.md`.

### Parsing

`parser.rs` is the starting point of the execution. It takes the binary file and makes sure that the header is valid
and matches the expectation of the current VM version. This is done by the `verify_integrity` function.

Then the `parse_file` function takes over. Its job is to extract all the information about function declarations. This
information will then be returned and given to the interpreter.

### Instructions

The file `instruction.rs` contain a nice and compact overview of the currently supported instruction set. The
instructions themselves are described in the specification.

### Builtins

BorealisVM provides two built-in functions. Their implementation can be found in the `builtins.rs` file. Builtin
functions operate on the stack directly. As such they take a reference to the stack and have no return value. Much like
regular function, they're expected to push any return values onto the stack.

An observant reader might notice that the `println` function pushes zero onto the stack as its return value, even though
its type is `Unit`. This is done because `Unit` is a valid assignment value in Clam. As such a statement like
`let a = println(3);` is perfectly valid. Because of this, a dummy value must be pushed onto the stack.

### Interpreter

The file `interpreter.rs` contains the actual bytecode interpreter. The interpreter takes a reference to the bytecode,
with the header removed and the list of parsed information about the declarations.

#### Stacks
The interpreter itself follows a 3-stack model. It has an operator stack, a local variable stack and a call stack.

The operator stack stores the operators of expressions and return values of functions.

The local variable stack's head automatically skips back at the end of a function call, effectively allowing the local
variables of the called function to be overwritten. This acts as a very primitive way of memory management.

The call stack is important for function calls. Before a function can call another function, all of the information
necessary to continue the execution of the calling function is saved on the call stack. After the called function
finishes execution, putting its return value on the top of the stack, the calling function pops its information from
the top of the call stack, restoring its context and resuming its execution. This of course works with recursive
and self-recursive calls as well. The information stored on the call stack is: the return address, frame pointer (Which
is the beginning address of the function's local variables on the local variable stack) and the declaration index (The
index of the calling function in the table of parsed declarations. Since this table stores useful information about
the calling function, it's much easier to just save the index on the stack, than saving all of the data kept in the
declaration table.)

The machine uses switch-based dispatch for instructions, with the large mach statement that takes up most of the
`interpret` function's body.

We're now ready to look at the implementation of some instructions. We will handpick a few particularly interesting
instructions, but encourage interesting readers to take a look at the whole instruction set, since it's not extremely
complicated.

#### Chosen Instructions

##### Bpush

```rust
Instruction::BPush => {
    let val = self.code[pc + 1] as f32;
    pc += 1;

    stack.push(val);
}
```
Inspired by the JVM, the `bpush` instruction allows us to push smaller, round values onto the stack efficiently,
requiring only one parameter instead of the 4 bytes that would be needed to properly represent a floating point number.

`bpush` is an instruction with a single byte parameter. We therefore get the next byte from the code and increment the
program counter to make sure we skip this byte. (The program counter is advanced once again at the end of the match
block.) Finally, this value is simply pushed onto the stack.

##### FAdd
```rust
Instruction::FAdd => {
    let f = stack.pop().unwrap();
    let s = stack.pop().unwrap();
    stack.push(f + s);
}
```

The add instruction pops two values off the stack, adds them together and pushes the result back onto the stack. Unlike
most common virtual machines, the current reference implementation assumes that the bytecode is correct, hence we use
Rust's `.unwrap()` method to unpack an optional value. This leads to a runtime exception for malformed bytecode. Proper
error handling is planned in the future.

##### VarBCSet

```rust
Instruction::VarBCSet => {
    let idx = self.code[pc + 1] as usize;
    let b_const = self.code[pc + 2] as f32;
    pc += 2;

    locals[fp + idx] = b_const;
}
```

Fast-track variable assignment operation. The reasoning behind the instruction is described in detail in the VM
specification.

We grab the index of the local variable to set (with respect to the current function's frame pointer), the byte
constant to assign to the variable, and we carry the assignment out in a single instruction.

##### FCmpEq

```rust
Instruction::FCmpEq => {
    let f = stack.pop().unwrap();
    let s = stack.pop().unwrap();

    if s == f {
        stack.push(1f32);
    } else {
        stack.push(0f32);
    }
}
```

The implementation of a comparison instruction. Instead of using a separate flag register, the value is pushed onto the
stack.

##### JmpOne

```rust
Instruction::JmpOne => {
    let new_pc = self.declarations[decl_idx].starting_address + self.code[pc + 1] as usize;
    pc += 1;

    let f = stack.pop().unwrap();

    if f == 1f32 {
        pc = new_pc;
        continue;
    }
}
```

The conditional jump instruction (with respect to the current function's starting address in bytecode). The relative
address that is to be jumped to is read from the code and converted into an absolute address by adding the starting
address of the function. This information is kept in the bytecode and is available to the VM.

Then, a check is made to see if there's 1 on the top of the stack, if so, we jump to the desired address by changing
the program counter.

##### Invoke

```rust
Instruction::Invoke => {
    let new_dec_idx = self.code[pc + 1] as usize;

    context.push(pc + 2);
    context.push(fp);
    context.push(decl_idx);

    fp += self.declarations[decl_idx].max_vars + 1;
    decl_idx = new_dec_idx;
    pc = self.declarations[decl_idx].starting_address;

    let params = self.declarations[decl_idx].param_count;
    for i in 0..params {
        locals[fp + (params - 1 - i)] = stack.pop().unwrap();
    }
    continue;
}
```

Perhaps the most complex instruction in the set. Before a function call, the context stack must be updated with the
current function's activation record so that we can restore the function's context upon returning control to it. In
Borealis, we push the return address, the function's frame pointer and its index in the function table. This information
is enough to fully restore a function's context.

Afterwards, the frame pointer is incremented. We know the maximum number of variables a function can have, allowing us
to easily move the frame pointer to an area we know will be free. We set the internal tracker of the currently executing
function (`decl_idx`) to the index of the new function, and we set the program counter to the new function's starting
address.

As the last step, we copy in the function's arguments into its local variable stack, after which the function may
proceed with its execution. 

##### BInvoke

```rust
Instruction::BInvoke => {
    let fn_id = self.code[pc + 1] as usize;
    pc += 1;

    BUILTINS[fn_id](&mut stack)
}
```

A less complex instruction that invokes a builtin. We simply get the id of the function we desire to call, fetch it from
the builtin function table and call it, giving it a reference to the stack.

##### Return

```rust
Instruction::Return => {
    // Main returned.
    if decl_idx == 0 {
        break;
    }

    let ret_dec = context.pop().unwrap() as usize;
    let ret_fp = context.pop().unwrap() as usize;
    let ret_pc = context.pop().unwrap() as usize;

    decl_idx = ret_dec;
    fp = ret_fp;
    pc = ret_pc;
    continue;
}
```

The return instruction pops the activation frame of the calling procedure from the context stack and restores it. If,
however, the main function returns, the execution simply ends. The main function is guaranteed to have the index 0
according to the specification.

With that, we end our tour of the chosen instructions.

### Further work

Given that this is a university project, it was not given nearly as much love as it should have been. Due to a lack of
time, the language has been kept relatively simple and the design suffered a bit as well. The virtual machine itself
takes many shortcuts that don't ensure optimality.

Future plans for the stack-machine include proper error handling and a test suite to ensure correct functionality. Plans
for the project itself can be found in the README of this project's sister project
[Clam](https://gitlab.com/arrem/Clam#future-work), which is the first language to compile to BorealisVM bytecode.