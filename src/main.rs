extern crate borealis_vm as borealis;
extern crate clap;

use clap::App;
use clap::Arg;

fn main() {
    let matches = App::new("Borealis VM")
        .version("0.1.0")
        .about("Stack-based mini Abstract Machine")
        .author("Alem Župa")

        .arg(Arg::with_name("execute")
            .short("e")
            .long("exec")
            .value_name("FILE")
            .help("Executes the specified *.ab file.")
            .takes_value(true)
            .required(true)
        )

        .get_matches();

    // Guaranteed not to be None, because execute is required.
    let file = matches.value_of("execute").unwrap();

    if let Err(e) = borealis::run(file) {
        println!("File could not be run successfully.");
        println!("Cause: {}", e.description());
    }
}
