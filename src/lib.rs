#[macro_use]
extern crate log;
extern crate time;
extern crate clap;

mod parser;
mod interpreter;

use std::error::Error;
use std::fs::File;
use std::io::Read;

pub fn run(file_path: &str) -> Result<(), Box<Error>> {
    let mut file = File::open(file_path)?;
    let mut bytes: Vec<u8> = Vec::new();
    file.read_to_end(&mut bytes)?;

    let (start, declarations) = parser::parse(&bytes)?;

    let interpreter = interpreter::Interpreter::new(&bytes[start..], declarations);

    let now = time::now();
    interpreter.interpret();
    let end = time::now();

    println!("Executed in: {}ms", (end - now).num_milliseconds());

    Ok(())
}


