mod declaration;
mod errors;
mod parser;

pub use self::declaration::Declaration;
pub use self::parser::parse;