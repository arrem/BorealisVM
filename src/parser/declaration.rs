#[derive(Debug)]
pub struct Declaration {
    pub max_vars: usize,
    pub param_count: usize,
    pub starting_address: usize,
}

impl Declaration {
    pub fn new(max_vars: usize, param_count: usize, starting_address: usize) -> Self {
        Declaration { max_vars, param_count, starting_address }
    }
}