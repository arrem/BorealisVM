use parser::Declaration;
use parser::errors;
use std::error::Error;

const HEADER_SIZE: usize = 7;
const MAGIC: [u8; 4] = [ 0x3D, 0x62, 0x16, 0x62 ];
const VERSION: [u8; 2] = [ 0x0, 0x8 ];
const HEADER_END: u8 = 0xFA;

pub fn parse(bytes: &[u8]) -> Result<(usize, Vec<Declaration>), Box<Error>> {
    verify_integrity(bytes)?;

    parse_file(&bytes[HEADER_SIZE..])
}

fn verify_integrity(bytes: &[u8]) -> Result<(), Box<Error>> {
    if bytes.len() < HEADER_SIZE {
        return Err(Box::new(errors::FileFormatError::InvalidFormat));
    }

    // Check that the magic number matches.
    let magic = bytes.iter().take(4)
        .zip(MAGIC.iter())
        .all(|(&actual, &expected)| actual == expected);

    if !magic {
        return Err(Box::new(errors::FileFormatError::MagicMismatch));
    }

    // Check for version match, both major and minor.
    let version = bytes.iter().skip(4).take(2)
        .zip(VERSION.iter())
        .all(|(&actual, &expected)| actual == expected);

    if !version {
        return Err(Box::new(errors::FileFormatError::VersionMismatch));
    }

    // Verify that the header end byte is present.
    if bytes[6] != HEADER_END {
        return Err(Box::new(errors::FileFormatError::InvalidFormat))
    }

    Ok(())
}

fn parse_file(bytes: &[u8]) -> Result<(usize, Vec<Declaration>), Box<Error>> {
    let mut result = Vec::new();

    let mut iter = bytes.iter().enumerate().peekable();

    let mut max_stack: u8;
    let mut param_count: u8;
    let mut starting_address = 0u32;

    if let Some(&(_, &n)) = iter.peek() {
        if n != 0xD0 {
            return Err(Box::new(errors::FileFormatError::InvalidFormat));
        }
    }

    // Function metadata:
    while let Some((idx, &byte)) = iter.next() {
        if byte == 0xD0 {
            // Max local var stack:
            if let Some((_, &n)) = iter.next() {
                max_stack = n;
            } else {
                return Err(Box::new(errors::FileFormatError::InvalidFormat));
            }

            // Param count:
            if let Some((_, &n)) = iter.next() {
                param_count = n;
            } else {
                return Err(Box::new(errors::FileFormatError::InvalidFormat));
            }

            // Starting address:
            for i in 0..4 {
                if let Some((_, &byte)) = iter.next() {
                    starting_address |= (u32::from(byte) << (8 * (3 - i))) as u32;
                } else {
                    return Err(Box::new(errors::FileFormatError::InvalidFormat));
                }
            }

            if let Some((_, &0xDE)) = iter.next() {
                result.push(Declaration::new(
                    max_stack as usize, param_count as usize,
                    starting_address as usize
                ));
            } else {
                return Err(Box::new(errors::FileFormatError::InvalidFormat));
            }
        } else if byte == 0xDF {
            return Ok((idx + HEADER_SIZE + 1, result));
        } else {
            return Err(Box::new(errors::FileFormatError::InvalidFormat));
        }
    }

    Err(Box::new(errors::FileFormatError::InvalidFormat))
}