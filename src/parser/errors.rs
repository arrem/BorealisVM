use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub enum FileFormatError {
    InvalidFormat,
    MagicMismatch,
    VersionMismatch,
}

impl fmt::Display for FileFormatError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::FileFormatError;

        match *self {
            FileFormatError::InvalidFormat   =>
                write!(f, "Invalid binary file format."),
            FileFormatError::MagicMismatch   =>
                write!(f, "Magic number mismatch."),
            FileFormatError::VersionMismatch =>
                write!(f, "Version mismatch."),
        }
    }
}

impl Error for FileFormatError {
    fn description(&self) -> &str {
        match *self {
            FileFormatError::InvalidFormat   =>
                "Invalid binary file format.",
            FileFormatError::MagicMismatch   =>
                "Magic number mismatch.",
            FileFormatError::VersionMismatch =>
                "Version mismatch."
        }
    }
}