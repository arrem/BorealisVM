extern crate rand;

mod builtins;
mod instruction;
mod interpreter;

pub use self::interpreter::Interpreter;