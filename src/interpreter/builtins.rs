use super::rand::{Rng, thread_rng};

pub const BUILTINS: [fn(&mut Vec<f32>); 2] = [
    println,
    random,
];

fn println(stack: &mut Vec<f32>) {
    println!("{}", stack.pop().unwrap());

    stack.push(0f32);
}

fn random(stack: &mut Vec<f32>) {
    stack.push(thread_rng().gen());
}