use std::mem;

#[allow(dead_code)  ]
pub enum Instruction {
    BPush    = 0,
    FPush    = 1,
    FStore   = 2,
    FLoad    = 3,

    FAdd     = 10,
    FSub     = 11,
    FMul     = 12,
    FDiv     = 13,
    FNeg     = 14,

    VarBCSet = 15,
    VarBCAdd = 16,
    VarBCSub = 17,
    VarBCMul = 18,
    VarBCDiv = 19,
    VarFCSet = 20,
    VarFCAdd = 21,
    VarFCSub = 22,
    VarFCMul = 23,
    VarFCDiv = 24,

    FCmpEq   = 30,
    FCmpNeq  = 31,
    FCmpLe   = 32,
    FCmpLeq  = 33,
    FCmpGe   = 34,
    FCmpGeq  = 35,

    Jmp      = 40,
    JmpOne   = 41,
    JmpZero  = 42,

    Invoke   = 50,
    BInvoke  = 51,
    Return   = 52,
}

impl From<u8> for Instruction {
    fn from(u: u8) -> Self {
        unsafe { mem::transmute(u ) }
    }
}