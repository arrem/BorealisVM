use parser::Declaration;
use interpreter::instruction::Instruction;
use interpreter::builtins::BUILTINS;

pub struct Interpreter<'code> {
    code: &'code [u8],
    declarations: Vec<Declaration>,
}

impl<'code> Interpreter<'code> {
    pub fn new(code: &'code [u8], declarations: Vec<Declaration>) -> Self {
        Interpreter { code, declarations }
    }

    pub fn interpret(&self) {
        if self.declarations.is_empty() {
            return;
        }

        let mut stack: Vec<f32> = Vec::with_capacity(50);
        let mut locals: Vec<f32> = Vec::with_capacity(2000);
        let mut context: Vec<usize> = Vec::with_capacity(20);

        for _ in 0..2000 {
            locals.push(0f32);
        }

        let mut pc = 0;
        let mut fp = 0;
        let mut decl_idx = 0;

        while pc < self.code.len() {
            let i = self.code[pc];
            match Instruction::from(i) {
                Instruction::BPush => {
                    let val = self.code[pc + 1] as f32;
                    pc += 1;

                    stack.push(val);
                }

                Instruction::FPush => {
                    let val = self.get_const(pc);
                    pc += 4;

                    stack.push(val);
                }

                Instruction::FStore => {
                    let idx = self.code[pc + 1] as usize;
                    pc += 1;

                    locals[fp + idx] = stack.pop().unwrap();
                }

                Instruction::FLoad => {
                    let idx = self.code[pc + 1] as usize;
                    pc += 1;

                    stack.push(locals[fp + idx]);
                }

                Instruction::FAdd => {
                    let f = stack.pop().unwrap();
                    let s = stack.pop().unwrap();
                    stack.push(f + s);
                }

                Instruction::FSub => {
                    let f = stack.pop().unwrap();
                    let s = stack.pop().unwrap();
                    stack.push(s - f);
                }

                Instruction::FMul => {
                    let f = stack.pop().unwrap();
                    let s = stack.pop().unwrap();
                    stack.push(f * s);
                }

                Instruction::FDiv => {
                    let f = stack.pop().unwrap();
                    let s = stack.pop().unwrap();
                    stack.push(s / f);
                }

                Instruction::FNeg => {
                    let f = stack.pop().unwrap();
                    stack.push(-f);
                }

                Instruction::VarBCSet => {
                    let idx = self.code[pc + 1] as usize;
                    let b_const = self.code[pc + 2] as f32;
                    pc += 2;

                    locals[fp + idx] = b_const;
                }

                Instruction::VarBCAdd => {
                    let idx = self.code[pc + 1] as usize;
                    let b_const = self.code[pc + 2] as f32;
                    pc += 2;

                    locals[fp + idx] += b_const;
                }

                Instruction::VarBCSub => {
                    let idx = self.code[pc + 1] as usize;
                    let b_const = self.code[pc + 2] as f32;
                    pc += 2;

                    locals[fp + idx] -= b_const;
                }

                Instruction::VarBCMul => {
                    let idx = self.code[pc + 1] as usize;
                    let b_const = self.code[pc + 2] as f32;
                    pc += 2;

                    locals[fp + idx] *= b_const;
                }

                Instruction::VarBCDiv => {
                    let idx = self.code[pc + 1] as usize;
                    let b_const = self.code[pc + 2] as f32;
                    pc += 2;

                    locals[fp + idx] /= b_const;
                }

                Instruction::VarFCSet => {
                    let idx = self.code[pc + 1] as usize;
                    let b_const = self.get_const(pc + 1);
                    pc += 5;

                    locals[fp + idx] = b_const;
                }

                Instruction::VarFCAdd => {
                    let idx = self.code[pc + 1] as usize;
                    let b_const = self.get_const(pc + 1);
                    pc += 5;

                    locals[fp + idx] += b_const;
                }

                Instruction::VarFCSub => {
                    let idx = self.code[pc + 1] as usize;
                    let b_const = self.get_const(pc + 1);
                    pc += 5;

                    locals[fp + idx] -= b_const;
                }

                Instruction::VarFCMul => {
                    let idx = self.code[pc + 1] as usize;
                    let b_const = self.get_const(pc + 1);
                    pc += 5;

                    locals[fp + idx] *= b_const;
                }

                Instruction::VarFCDiv => {
                    let idx = self.code[pc + 1] as usize;
                    let b_const = self.get_const(pc + 1);
                    pc += 5;

                    locals[fp + idx] /= b_const;
                }

                Instruction::FCmpEq => {
                    let f = stack.pop().unwrap();
                    let s = stack.pop().unwrap();

                    if s == f {
                        stack.push(1f32);
                    } else {
                        stack.push(0f32);
                    }
                }

                Instruction::FCmpNeq => {
                    let f = stack.pop().unwrap();
                    let s = stack.pop().unwrap();

                    if s != f {
                        stack.push(1f32);
                    } else {
                        stack.push(0f32);
                    }
                }

                Instruction::FCmpLe => {
                    let f = stack.pop().unwrap();
                    let s = stack.pop().unwrap();

                    if s < f {
                        stack.push(1f32);
                    } else {
                        stack.push(0f32);
                    }
                }

                Instruction::FCmpLeq => {
                    let f = stack.pop().unwrap();
                    let s = stack.pop().unwrap();

                    if s <= f {
                        stack.push(1f32);
                    } else {
                        stack.push(0f32);
                    }
                }

                Instruction::FCmpGe => {
                    let f = stack.pop().unwrap();
                    let s = stack.pop().unwrap();

                    if s > f {
                        stack.push(1f32);
                    } else {
                        stack.push(0f32);
                    }
                }

                Instruction::FCmpGeq => {
                    let f = stack.pop().unwrap();
                    let s = stack.pop().unwrap();

                    if s >= f {
                        stack.push(1f32);
                    } else {
                        stack.push(0f32);
                    }
                }

                Instruction::Jmp => {
                    let new_pc = self.declarations[decl_idx].starting_address + self.code[pc + 1] as usize;
                    pc = new_pc;
                    continue;
                }

                Instruction::JmpOne => {
                    let new_pc = self.declarations[decl_idx].starting_address + self.code[pc + 1] as usize;
                    pc += 1;

                    let f = stack.pop().unwrap();

                    if f == 1f32 {
                        pc = new_pc;
                        continue;
                    }
                }

                Instruction::JmpZero => {
                    let new_pc = self.declarations[decl_idx].starting_address + self.code[pc + 1] as usize;
                    pc += 1;

                    let f = stack.pop().unwrap();

                    if f == 0f32 {
                        pc = new_pc;
                        continue;
                    }
                }

                Instruction::Invoke => {
                    let new_dec_idx = self.code[pc + 1] as usize;

                    context.push(pc + 2);
                    context.push(fp);
                    context.push(decl_idx);

                    fp += self.declarations[decl_idx].max_vars + 1;
                    decl_idx = new_dec_idx;
                    pc = self.declarations[decl_idx].starting_address;

                    let params = self.declarations[decl_idx].param_count;
                    for i in 0..params {
                        locals[fp + (params - 1 - i)] = stack.pop().unwrap();
                    }
                    continue;
                }

                Instruction::BInvoke => {
                    let fn_id = self.code[pc + 1] as usize;
                    pc += 1;

                    BUILTINS[fn_id](&mut stack)
                }

                Instruction::Return => {
                    // Main returned.
                    if decl_idx == 0 {
                        break;
                    }

                    let ret_dec = context.pop().unwrap() as usize;
                    let ret_fp = context.pop().unwrap() as usize;
                    let ret_pc = context.pop().unwrap() as usize;

                    decl_idx = ret_dec;
                    fp = ret_fp;
                    pc = ret_pc;
                    continue;
                }
            }

            pc += 1;
        }
    }

    fn get_const(&self, pc: usize) -> f32 {
        let b1 = self.code[pc + 1] as u32;
        let b2 = self.code[pc + 2] as u32;
        let b3 = self.code[pc + 3] as u32;
        let b4 = self.code[pc + 4] as u32;

        let bits: u32 = (b1 << 24) | (b2 << 16) | (b3 << 8) | b4;

        f32::from_bits(bits)
    }
}