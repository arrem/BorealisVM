# Borealis VM design notes

Notes about the design of the VM will be held in this document. It is in no way intended
to be a full specification of the virtual machine or to give
the reader enough information to implement the VM on their own.

A full specification of the VM may come at a later point.

## File structure

The recommended file extension for a Borealis VM file is `.ab`.

Each file contains a header that consists of:
* The magic number - `0x3D621662`
* Major version of the binary, currently `0`
* Minor version of the binary, currently `8`
* Header end byte - `0xFA`

The header always comes first in a `.ab` file. It is then followed by the function declaration block. This block
comes directly after the header and contains the necessary information for all the functions that are used in the
file, following the format that we describe in this section.

A function declaration list is a list of function declarations that come one after another. Each function declaration
follows the following format:
* Function declaration start byte - `0xD0`
* Maximal number of local variables, as a byte.
* The number of parameters of the function, as a byte.
* The starting address of the function's body in code, as a 4 byte integer, specified in the big endian order.
* Function declaration end byte - `0xDE`

After all of the declarations have been listed, there must come a declaration list end byte - `0xDF`. Note that the
body of the function isn't contained in the declaration section. Function bodies all come at a later point.

**Note:** The main function, from which the actual execution begins, must always be placed first in the function
declaration list.

**Note:** When determining the starting address of a function body in code, the file header and the function declaration
block are ignored. As such, the first instruction of the main function has the address `0`.

#### Example
The following example demonstrates how the bookkeeping information of a `.ab` file would look. The individual bytes are
represented as series of two hexadecimal digits. Comments about the code have been prefixed with `#` and may be ignored,
as well as any whitespace. Actual bytecode is not allowed to contain comments.
```
# Magic     Vers. Header end
3D 62 16 62 00 07 FA

# Declaration of the main function.
D0
# Having one local variable and taking no arguments.
01 00
# With its first instruction at the address 0.
00 00 00 00
# End declaration.
DE

# Declare a second function, having one argument.
D0 01 01
# With its first instruction at the address 7.
00 00 00 07
# End declaration.
DE

#End function declaration block.
DF
```
Immediately after the block we defined above come the bodies of the declared functions, the first one being the main
function. This however has been left out of the example, since the actual instruction set is assumed to be unfamiliar
to the reader. The example will be completed at the end of the next section.

## Instruction set

**Short format description and introduction:**

### Instruction name | \<opcode\> \[optional arguments\]

Every instruction has an user friendly name which is used to identify the instruction more easily.
For all other purposes, this name is irrelevant.

Then follows the opcode of the instruction, which is used to represent it in the bytecode. After the
opcode we specify list of arguments for the particular instruction. These follow the opcode
directly. Not all instructions have arguments, but for any instruction that needs
an argument, the argument is mandatory. Similarly, additional arguments must not be provided for
instructions that do not require them. All arguments are (unsigned) byte values, just like
all of the opcodes.

## Variable and stack manipulation

### b_push | 0x00 \<byte\>

Pushes the specified byte constant onto the top of the operand stack. Used to quickly push constants
ranging from 0 to 255 inclusive. The specified byte will be converted to a floating point value and
pushed on top of the stack.

This of course makes this instruction more efficient than the `f_push` instruction.

#### Examples

`0x00 0x10` - pushes the constant 16 onto the stack.

### f_push | 0x01 \<b1\> \<b2\> \<b3\> \<b4>

An instruction for pushing floating point constants onto the operand stack. Requires four
byte argument, which represent the bytes of the IEEE 754 single-precision floating-point number,
stored in a big endian order.

#### Examples

`0x01 0x40 0xa0 0x00 0x00` - pushes the constant 5.0 to the stack.

### f_store | 0x02 <var_idx>

Pops a value from the top of the stack and stores it into the local variable indexed by the
specified index position. The stack must not be empty when this instruction is run. Behavior
in such a case is undefined and the VM may crash.

#### Examples
`0x02 0x01` - pops the top value from the stack and stores it into the second local variable.

### f_load | 0x03 <var_idx>

Retrieves the value contained in the local variable at the specified index and pushes it onto
the operand stack. The value of the variable itself must remain unchanged in the process. Behavior
when the variable at the specified index was not previously initialized is left undefined.

The reference BorealisVM implementation initializes all local variables to zero by default.

#### Examples
`0x03 0x02` - puts the value of the third local variable onto the top of the stack.

## Arithmetic

### f_add | 0x0A, f_sub | 0x0B, f_mul | 0x0C, f_div | 0x0D

Adds, subtracts, multiplies or divides the top stack element by the second stack element, adding the
result back onto the stack.
Cases where the stack does not contain enough elements are left undefined.

### f_neg | 0x0E

Pops the value from the top of the stack, negates it and puts it back onto the stack. As with the
other instructions, it is left undefined for an empty stack.

## Fast variable operations

**Motivation:** Working with variables usually requires a relatively large number of operations. For example,
a statement like `a += 7` , would have to be translated as follows, under the assumption that `a` is the
first local variable:

```
f_load  0  # Push the current value of a onto the stack.
b_push  7  # Push the constant 7 onto the stack.
f_add      # Add the two elements from the top of the stack.
f_store 0  # Store the resulting value back into a. 
```

To avoid using four instructions for simple constant assignment, the VM supports additional operations that enable
the instructions above to be concisely rewritten as

```
var_fc_add 0 7  # Add 7 to the value of a.
```

**Note:** The current instruction set only supports such instructions with variable operands. This may change in the
future, and space has been reserved for such instructions to come without breaking backward compatibility.

## Function calls

As specified earlier, to call a function in Borealis, one uses the `invoke` or `b_invoke` instructions. Both of these
have an additional byte argument that references the function that is to be called.

This section aims to describe the details of how function calls are handled in bytecode and what the VM does internally
when a function call occurs.

Every function, whether built-in or user-defined takes its arguments from the stack. The caller of the function must
assure that the correct arguments for the function are located on the stack before calling the invoke instruction. The
arguments are pushed in the order they appear in within the function itself. When the invoke instruction is called, the
VM pops the parameters from the stack, and moves them onto the function's local variable stack.

This is also how functions access their local variables. The reserved space for local variables is the sum of the number
of arguments a function needs, and the maximum amount of local variables it declares. As such, the arguments occupy the
lower positions. A function `foo(bar: float, baz: float)` would reference its first argument, `bar`, 
a its 0th local variable. The argument `baz` would then be the 1st local variable of the function.

The invoke instructions take a byte argument that references the function to call. For user-defined functions, this is
simply the index of the function in the function definition table, as specified at the start of a bytecode file. For
built-in functions, this is the `id` column of the table below.

BorealisVM currently supports two built-in functions. Built-in functions act similarly to the user-defined ones, with
the exception that they're invoked using the `b_invoke` instruction.


| id | Function definition  | Description   |
| --- | -------------------- |:-------------:|
| 0 | println(float): unit | Takes the given argument and prints it to `stdout`, appending a new line at the end. |
| 1 | random(): float      | Returns a randomly generated floating point number in the range \[0, 1\].     |